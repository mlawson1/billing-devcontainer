# billing-devcontainer

This is a Billing devcontainer aimed to get users up and working with automated scripts within two hours.

Disclaimer: This has only been tested on Mac where docker performance is poor. This might actually be faster on windows or linux.


## VSCode Prerequisites
1. Download the repo
2. Download [Docker Desktop](https://docs.docker.com/desktop/mac/install/) if you do not already have it installed
3. Open the repo in VSCode and click the icon in the lower left corner to open in Dev Container, wait for the container to load
4. Ensure you're on Zuora's VPN network, Maven repo sources will be auto added to the devcontainer

## Getting Started

Ensure you're on the vpn
Run ./jesus_take_the_wheel.sh 

In billing/build, run in terminal: ant clean generate
In billing root, run in terminal: mvn clean install -DskipTests
Wait 2 hours
In database/scripts, run in terminal: ./refresh-test-db.sh 

Add tomcat server extension (should be auto added) and create a new server, import the war file fro billing/servers/frontend/target
