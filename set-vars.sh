#!/bin/bash
CURRENT_RELEASE=320.0.0
RELEASE=$((${CURRENT_RELEASE:0:3}-1))
DB_WORKSPACE=$PWD/..
USE_MULTI_SHARD=true

#source db connection parameters
SHARD0_HOST=mysql
SHARD0_PORT=3306
SHARD0_USER=root
SHARD0_PASSWORD=
SHARD0_SCHEMA=zodiac

#ro db host and database name
RO_HOST=mysql
RO_PORT=3306
RO_SCHEMA=zodiac

# vars for create new shard
NEWSHARD_SOURCE_FILE=shard.sql
NEWSHARD_HOST=mysql
NEWSHARD_PORT=3306
NEWSHARD_USER=root
NEWSHARD_PASSWORD=
# default to null
NEWSHARD_SCHEMA=
NEWSHARD_SCHEMA_PREFIX=zodiac_shard_
# default to null
NEWSHARD_NAME=
# the initial auto_increment value is 100Billion, be careful to change it
NEWSHARD_AUTO_INCREMENT_INIT_VALUE=100000000000

#ro db host and database name
NEWSHARD_RO_HOST=mysql
NEWSHARD_RO_PORT=3306
NEWSHARD_RO_SCHEMA=

#GLOBAL DB connection parameters
GLOBAL_HOST=mysql
GLOBAL_PORT=3306
GLOBAL_USER=root
GLOBAL_PASSWORD=
GLOBAL_SCHEMA=zodiac_global

#QUARTZ DB connection parameters
QUARTZ_HOST=mysql
QUARTZ_PORT=3306
QUARTZ_USER=root
QUARTZ_PASSWORD=
QUARTZ_SCHEMA=zodiac_quartz

NEWSHARD_NEXT_SHARDID=1
ORIG_SHARD_NEXT_SHARDID=0 #for split shard from none-shard1

NEWSHARD_MYSQL_PARAMS=" --user=$NEWSHARD_USER --password=$NEWSHARD_PASSWORD --host=$NEWSHARD_HOST --port=$NEWSHARD_PORT "
SHARD0_MYSQL_PARAMS=" --user=$SHARD0_USER --password=$SHARD0_PASSWORD --host=$SHARD0_HOST --port=$SHARD0_PORT "
GLOBAL_MYSQL_PARAMS=" --user=$GLOBAL_USER --password=$GLOBAL_PASSWORD --host=$GLOBAL_HOST --port=$GLOBAL_PORT "
QUARTZ_MYSQL_PARAMS=" --user=$QUARTZ_USER --password=$QUARTZ_PASSWORD --host=$QUARTZ_HOST --port=$QUARTZ_PORT "

#file store configuration
REPOSITORY_TYPE=repository.local
REPOSITORY_LOCAL_DIR=/tmp/repository

TENANT_CATEGORIES=0
